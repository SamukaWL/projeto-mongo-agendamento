const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const app = express();
const routes = require("./routes");

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.set("views", path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

mongoose.connect("mongodb://localhost:27017/agendamento",
{useNewUrlParser: true, useUnifiedTopology: true});
mongoose.set('useFindAndModify', false);

app.use("/", routes);


module.exports = app;