const appointmentModel = require("../model/Appointment");
const mongoose = require("mongoose");
const AppointmentFactory = require("../factories/AppointmentFactory");
const mailer = require("nodemailer");

const Appointment = mongoose.model("Appointment", appointmentModel);

class AppointmentService {

  async create(name, email, description, cpf, date, time) {
    try {
      const newAppointment = new Appointment({
        name, 
        email, 
        description, 
        cpf, 
        date, 
        time,
        finished: false,
        notified: false
      });
      await newAppointment.save();
      return true;
    
    } catch (error) {
      console.log(error);
      return false;
    }

  }

  async getAll(showFinished) {

    if(showFinished) {
      return await Appointment.find(); 
    }
    
    const arrAppointments = await Appointment.find({"finished": false});
    const appointments = [];

    arrAppointments.forEach(appointment => {
      if(appointment.date != undefined) {
        appointments.push(AppointmentFactory.build(appointment));
      }
    });

    return appointments;

  }

  async getById(id) {
    try {
      const event = await Appointment.findOne({'_id': id});
      return event;
    } catch (error) {
      console.log(error);
    }
  }

  async Finish(id) {
    try {
      await Appointment.findByIdAndUpdate(id, {"finished": true});
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  async search(query) {
    try {
      const appos = await Appointment.find().or([{email: query}, {cpf: query}]);
      return appos;
    } catch (error) {
      console.log(error);
      return [];
    }
  }

  async SendNotification() {
    const transporter = mailer.createTransport({
      host: "smtp.mailtrap.io",
      port: 25,
      auth: {
        user: "039ccdab376920",
        password: "601728508431ab",
        auth: "PLAIN"
      }
    })

    const appos = await this.getAll(false);
    appos.forEach(async app => {
      let date = app.start.getTime();
      const hour = 1000 * 60 * 60;
      let gap = date - Date.now();

      if(gap <= hour) {
        
        if(!app.notified) {
          //await Appointment.findByIdAndUpdate(app.id, {notified: true});

          transporter.sendMail({
            from: "Samuel <samuka@mail.com.br>",
            to: app.email,
            subject: "Sua consulta vai acontecer em breve",
            text: "Lorem impsun"
          }).then(() => {
            
          }).catch(err => {
            console.log(err);
          })

        }

      }
    })
   
  }

}

module.exports = new AppointmentService();

