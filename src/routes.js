const express = require("express");
const router = express.Router();

const appointmentService = require("./service/AppointmentService");

router
  .get("/", (req, res) => res.render("index"))
  .get("/create", (req, res) => res.render("create"))
  .post("/create", async (req, res) => {
    const {name, email, description, cpf, date, time} = req.body;
    
    const status = await appointmentService.create(
      name, 
      email, 
      description, 
      cpf, 
      date, 
      time
      );

      if(!status) {
        res.send("Oops...Ocorreu uma falha :(");
      }

      res.redirect("/");
  })
  .get("/getcalendar", async (req, res) => {
    const appointments = await appointmentService.getAll(false);
    res.json(appointments);
  })
  .get("/event/:id", async (req, res) => {
    const appo = await appointmentService.getById(req.params.id);
    res.render("event", { appo });
  })
  .post("/finish", async (req, res) => {
    const id = req.body.id;
    await appointmentService.Finish(id);

    res.redirect("/");
  })
  .get("/list", async (req, res) => {
    const appos = await appointmentService.getAll(true);
    res.render("list", {appos});
  })

  .get("/searchresult", async (req, res) => {
    try {
      const appos = await appointmentService.search(req.query.search)
      res.render("list", { appos });
    } catch (error) {
      console.log(error);
    }
  })

  const pollTime = 5000;

  setInterval(async () => {
    await appointmentService.SendNotification();
  }, pollTime)

module.exports = router;